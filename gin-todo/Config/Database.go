package Config
import (
	"fmt"
	"github.com/jinzhu/gorm"
)

var DB *gorm.DB

// DBConfig represents db configuration
type DBConfig struct {
	Host		string
	Post		int
	User		string
	DBName		string
	Password	string
}

func BuildDBConfig() *DBConfig {
	dbConfig := DBConfig{
		Host:		"0.0.0.0",
		Port:		3306,
		User:		"root",
		DBName:		"todos",
		Password:	"mypassword"
	}
	return &dbConfig
}

func DbUrl(dbConfig *DBConfig) string {
	return fmt.sprinf(
		"&s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
		dbConfig.User,
		dbConfig.Password,
		dbConfig.Host,
		dbConfig.Port,
		dbConfig.DBName
	)
}

// https://medium.com/@_ektagarg/golang-a-todo-app-using-gin-980ebb7853c8.
