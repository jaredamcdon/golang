# Go(lang) Notes
### go needs a $GOPATH and $GOROOT
- To show these run:
  - `go env GOPATH`
  - `go env GOROOT`

### Go Package Management
- Go needs a go.mod to store package versioning
  - to initialize this you need the following:
    - command: `go mod init {main.go}`
    - command: `go mod tidy`
    - once a go.mod file exists run `go get {package}` to install all packages for the application
