package main

import (
	"database/sql"
	"errors"
	"encoding/json"
	"fmt"
	"io/ioutil"
	//"strconv"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

var (
	ErrDuplicate	= errors.New("record already exists")
	ErrNotExists	= errors.New("row not exists")
	ErrUpdateFailed	= errors.New("update failed")
	ErrDeleteFailed	= errors.New("delete failed")
)

//jsoning
type Translations struct {
	Translations []Translation `json:"e2ms"`
}

type Translation struct {
	English		string `json:"e"`
	Minionspeak	string `json:"ms"`
}

func main() {

//create dictFile from "dict.json"
	dictFile, err := os.Open("dict.json")
	if err != nil {
		fmt.Println(err)
	}
	//defer closing file so we can use it
	defer dictFile.Close()

	// read our opened jsonFile as a byte array
	byteValue, _ := ioutil.ReadAll(dictFile)
	var translations Translations
	//unmarshall
	json.Unmarshal(byteValue, &translations)

	//sqliting

	const sqlFile = "translate.db"
	db, err := sql.Open("sqlite3", sqlFile)
	defer db.Close()
	createQuery:= `
		CREATE TABLE IF NOT EXISTS minionTranslate(
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			english TEXT NOT NULL UNIQUE,
			minionSpeak TEXT NOT NULL
		);
	`

	db.Exec(createQuery)

	insertable := ""
	for i:= 0; i < len(translations.Translations); i++ {
		insertable = "INSERT INTO minionTranslate(english, minionSpeak) values('" + translations.Translations[i].English + "','" + translations.Translations[i].Minionspeak + "');\n"
		db.Exec(insertable)
	}

}